<?php

return [
    'propel' => [
        'database' => [
            'connections' => [
                'WsClientConnection' => [
                    'adapter'    => 'mysql',
                    'classname'  => 'Propel\Runtime\Connection\ConnectionWrapper',
                    'dsn'        => 'mysql:host=localhost;dbname=zonestockage',
                    'user'       => 'root',
                    'password'   => 'root',
                    'attributes' => []
                ]
            ]
        ],
                'general' => [
            'project' => 'ws-zone-stockage'
        ],
        'paths' => [
            'projectDir' => '/var/www/html/ws-zone-stockage',
            'schemaDir' => '/var/www/html/ws-zone-stockage/script',
            'phpDir' => '/var/www/html/ws-zone-stockage/module/WsClient/src'
        ],
        'runtime' => [
            'defaultConnection' => 'WsClientConnection',
            'connections' => ['WsClientConnection']
        ],
        'generator' => [
            'defaultConnection' => 'WsClientConnection',
            'connections' => ['WsClientConnection']
        ]
    ]
];


