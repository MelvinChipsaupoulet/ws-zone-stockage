<?php

/* return array(
    'router' => array(
        'routes' => array(
            'rWsClient' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/wsClient',
                    'constraints' => array(
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]+',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'WsClient\Controller',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'rWsClientChild' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/:controller',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]+',
                            ),
                            'defaults' => array(
                                '__NAMESPACE__' => 'WsClient\Controller',
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'controllers' => array(//Controller
        'invokables' => array(//Déclaration des controleurs dynamiquement initialisables
            'Connection' => 'WsClient\Controller\ConnectionController',
            'Reservation' => 'WsClient\Controller\ReservationController',
        ),
    ),
    // Cette section se trouve au même niveau hiérarchique que la section 'router'
    'view_manager' => array(//Gestionnaire de vue
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),
);
*/

return array(
    'router' => array(
        'routes' => array(
            'rWsClient' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/wsClient',
                    'constraints' => array(
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]+',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'WsClient\Controller',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'rWsClientChild' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/:controller',
                            'constraints' => array(
                                'controller' => '[a-zA-Z0-9_-]+',
                            ),
                            'defaults' => array(
                                '__NAMESPACE__' => 'WsClient\Controller',
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Connection' => 'WsClient\Controller\ConnectionController',
            'Reservation' => 'WsClient\Controller\ReservationController',
        ),
    ),
    'view_manager' => array(
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),
);

