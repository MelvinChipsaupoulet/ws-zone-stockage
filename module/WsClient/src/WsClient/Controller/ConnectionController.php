<?php

namespace WsClient\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use WsClient\Model\UtilisateurQuery;
use Zend\Session\Container;

class ConnectionController extends AbstractRestfulController 
{
   
    
    public function getList() {
       return new JsonModel(array(
            'connected' => 'test'
        ));   
    }
    
    private function cryptToMySqlPassword($password) {
        $pass = strtoupper(sha1(sha1($password, true)));
        $pass = '*'. $pass;
        return $pass;
    }
    
    public function create($data) {
        
        $resultat = false;
        $login = $data['login'];
        $password = $data['password'];
        $cryptPassword = $this->cryptToMySqlPassword($password);
        $utilisateur = UtilisateurQuery::create()->findOneByArray(array(
            'login' => $login,
            'password' => $cryptPassword,
            'type' => 'client',
                    )
             );
        if($utilisateur != null) {
            $container = new Container('utilisateur');
            $container->client = $utilisateur;
            $resultat = true;
        }
        return new JsonModel(array(
            'connected' => $resultat
        ));                  
    }
    
    
}
